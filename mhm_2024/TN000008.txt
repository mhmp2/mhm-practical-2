[Section Optotrak]
Enable=TRUE
Markers Port 1=10
Markers Port 2=0
Markers Port 3=0
Markers Port 4=0
Buffer Data Type=32
Collection Time=15.000000
Frame Frequency=200
Marker frequency=2500.000000
Voltage=7.000000
Duty Cycle=0.300000
RT 3D Feedback=TRUE
RT Convert On Host=TRUE
Interpolation=FALSE
External Clock=FALSE
External Trigger=FALSE
Use Simulation Lib=FALSE
Use Standard Cam=FALSE
Use Old API=FALSE
Camera File="/F/mhm_2024/mhm_2024.cam"
Old Camera File="/F/mhm_2024/mhm_2024.cam"
Feedback=TRUE
RT Block For 3D Frame=FALSE
RT Marker 1=0
RT Marker 2=0
RT Marker 3=0
RT Display Posture=TRUE
RT Display Marker Numbers=TRUE
RT Display Circles=TRUE
RT Posture Axes=0
RT Reference Posture=""

[Section Qualisys]
Address="127.0.0.1"
Port=22223
Markers=10
Sample Rate=200
Stream Rate="AllFrames"
Stream Data="\00\00\00\01\00\00\00\01"
Save Stream Data=FALSE
Stream All Markers=FALSE
Offline Feedback=TRUE
RT 3D Feedback=TRUE
RT Block For 3D=FALSE
RT Marker 1=0
RT Marker 2=0
RT Marker 3=0
Measurement Mode=1
Master=0
FBWM May Start=FALSE
FBWM May Stop=FALSE
QTM May Start=FALSE
QTM May Stop=FALSE

[Section AI]
AI Algorithm=FALSE
FP1 Channels="Dev1/ai16:22"
FP2 Channels="Dev1/ai23:29"
Generic Channels="<Array>\0D\0A<Name>Generic AI Channels Array</Name>\0D\0A<Dimsize>3</Dimsize>\0D\0A<Cluster>\0D\0A<Name>Generic Channel</Name>\0D\0A<NumElts>5</NumElts>\0D\0A<I32>\0D\0A<Name>AI.TermCfg</Name>\0D\0A<Val>10106</Val>\0D\0A</I32>\0D\0A<Boolean>\0D\0A<Name>Enable GAI </Name>\0D\0A<Val>0</Val>\0D\0A</Boolean>\0D\0A<DBL>\0D\0A<Name>Ai.Min</Name>\0D\0A<Val>-10.00000000000000</Val>\0D\0A</DBL>\0D\0A<DBL>\0D\0A<Name>Ai.Max</Name>\0D\0A<Val>10.00000000000000</Val>\0D\0A</DBL>\0D\0A<DAQChannel>\0D\0A<Name>Channel Name</Name>\0D\0A<Val>Dev1/ai3</Val>\0D\0A</DAQChannel>\0D\0A</Cluster>\0D\0A<Cluster>\0D\0A<Name>Generic Channel</Name>\0D\0A<NumElts>5</NumElts>\0D\0A<I32>\0D\0A<Name>AI.TermCfg</Name>\0D\0A<Val>10106</Val>\0D\0A</I32>\0D\0A<Boolean>\0D\0A<Name>Enable GAI </Name>\0D\0A<Val>1</Val>\0D\0A</Boolean>\0D\0A<DBL>\0D\0A<Name>Ai.Min</Name>\0D\0A<Val>-10.00000000000000</Val>\0D\0A</DBL>\0D\0A<DBL>\0D\0A<Name>Ai.Max</Name>\0D\0A<Val>10.00000000000000</Val>\0D\0A</DBL>\0D\0A<DAQChannel>\0D\0A<Name>Channel Name</Name>\0D\0A<Val>Dev1/ai2</Val>\0D\0A</DAQChannel>\0D\0A</Cluster>\0D\0A<Cluster>\0D\0A<Name>Generic Channel</Name>\0D\0A<NumElts>5</NumElts>\0D\0A<I32>\0D\0A<Name>AI.TermCfg</Name>\0D\0A<Val>10106</Val>\0D\0A</I32>\0D\0A<Boolean>\0D\0A<Name>Enable GAI </Name>\0D\0A<Val>0</Val>\0D\0A</Boolean>\0D\0A<DBL>\0D\0A<Name>Ai.Min</Name>\0D\0A<Val>-10.00000000000000</Val>\0D\0A</DBL>\0D\0A<DBL>\0D\0A<Name>Ai.Max</Name>\0D\0A<Val>10.00000000000000</Val>\0D\0A</DBL>\0D\0A<DAQChannel>\0D\0A<Name>Channel Name</Name>\0D\0A<Val>cDAQ1Mod2/ai2</Val>\0D\0A</DAQChannel>\0D\0A</Cluster>\0D\0A</Array>\0D\0A"
Enable FP1=TRUE
Enable FP2=TRUE
Enable Specific Channel=FALSE
Specific Channel="Dev1/ai1"
Term Cfg=10106
Min=-10.000000
Max=10.000000
Collection Time=5.000000
Scan Rate=200.000000
Force Plate Type="Dual belt"
Null Offset Method="UseMeanNull"
RT Feedback=FALSE
RT Feedback Rate=30
RT Channel 1=0
RT Channel 2=0
RT Channel 3=0
FP Feedback=TRUE
GAI Feedback=TRUE
SAI feedback=TRUE

[Section AO]
Channel 1="cDAQ1Mod1/ao0"
Low Value 1=0.000000
High Value 1=8.000000
Enable 1=TRUE
Channel 2="cDAQ1Mod1/ao1"
Low Value 2=0.000000
High Value 2=8.000000
Enable 2=TRUE
Generic AO Channels="Analog Output Channels"
Enable Generic AO Channels?=FALSE

[Section DI]
Channel 1="Dev1/port0/line0"
Enable 1=FALSE
Channel 2=""
Enable 2=FALSE
Channel 3=""
Enable 3=FALSE
Channel 4="Dev1/port0/line3"
Enable 4=FALSE
Nr Of Channels=0
Use DI Ports=FALSE
DI Ports="Dev1/port0"

[Section DO]
Channel 1="Dev1/port0/line0"
On Signal 1=0
Enable 1=FALSE
Channel 2=""
On Signal 2=0
Enable 2=FALSE
Channel 3=""
On Signal 3=0
Enable 3=FALSE
Channel 4="Dev1/port0/line3"
On Signal 4=0
Enable 4=FALSE
Nr Of Channels=0
Use DO Ports=FALSE
DO Ports="Dev1/port0"

[Section TCP]
Port=2222
Packet Frequency=0
Send Optotrak Data=FALSE
Send FP Data=FALSE
Send GAI Data=FALSE

[Section EMG]
Write Sync Pulse=FALSE

[Section Timer]
Use Counter on DAQ Card=FALSE
Counter="Dev1/ctr1"
Show Timer Feedback=FALSE

[Section Motion Capture]
MoCap=1
3D Algorithm=FALSE

[Section Application]
Name="HMS"