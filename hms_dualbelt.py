# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 16:57:35 2024
Collections of functions to (pre-)process the optotrack data coming out of the 
dualbelt treadmill lab 

@author: Koen Lemaire
"""

#%% load packages
# Load function
import numpy as np

#%% function definitions
def readndf(filename):
    """
    x,y,z,data,idx_good = readndf(filename)
    
   %READNDF Reads Optotrak NDF data files into Python
    Input:
        filename    = the filename (including path and .ndf-extension) of the datafile you want to load
    Output:
        x           = [n_marker x n_sample] x-coordinate marker positions [m]
        y           = [n_marker x n_sample] y-coordinate marker positions [m]
        z           = [n_marker x n_sample] z-coordinate marker positions [m]
        data        = [n_sample x n_dim x n_marker] marker positions [m] 
        idx_good    = bolean (n_sample,) array with False values for missing data
        
        ! note the unit (meters) of the outputted data
        
        Author: Koen Lemaire (k.k.lemaire2@vu.nl), based on code by Edwin Reuvers
    """
    
    
    # Load data
    with open(filename, 'rb') as f:
        filetype = np.fromfile(f,dtype=np.int8,count=1)
        nmarkers = int(np.fromfile(f,dtype=np.int8,count=1)) # number of markers used
        ndim = int(np.fromfile(f,dtype=np.int8,count=1,offset=1)) # number of dimensions measured
        data = np.fromfile(f,dtype=np.float32,offset=252) # data of marker positions
        
    # Reshape data
    Ncol = nmarkers*ndim
    Nrow = int(data.shape[0]/Ncol)
    data = data.reshape((Nrow,Ncol))
    
    # Extract x,y,z data
    x = data[:,0::ndim]/1000
    y = data[:,1::ndim]/1000
    z = data[:,2::ndim]/1000
    
    data=np.array([x,y,z])
    data=np.transpose(data, (1, 0, 2))
    
    # look for bogus samples
    idx_good=x>-1e10
    if idx_good.all():
        idx_good=None
    else:
        idx_good=np.sum(idx_good,axis=1)
        idx_good=idx_good==nmarkers     
        print("warning: missing marker data detected in file: "+filename)
     
    # Outputs x, y and z coordinates (nmarker x nsample) with respect to aligned reference frame in mm
    return x,y,z,data,idx_good
    

def rigid_transform_3D(A:np.ndarray, B:np.ndarray) -> (np.ndarray,np.ndarray): 
    """
    R,t = rigid_transform_3D(A, B)
    
    Inputs
    ----------
    A : [nx3] or [3xn] numpy array representing point cloud
    B : [nx3] or [3xn] numpy array representing point cloud

    Process
    ------
    Uses singular value decomposition to find the least squares optimal 
    [3x3] rotation matrix R and vector t such that RA+t=B  
    
    Returns
    -------
    R : 3x3 Rotation matrix
    t : 3x1 translation vector 
    
    Author: Koen Lemaire (k.k.lemaire2@vu.nl)
    """
    assert A.shape == B.shape

    num_rows, num_cols = A.shape
    if num_rows != 3:
        raise Exception(f"matrix A is not 3xN, it is {num_rows}x{num_cols}")

    num_rows, num_cols = B.shape
    if num_rows != 3:
        raise Exception(f"matrix B is not 3xN, it is {num_rows}x{num_cols}")

    # find mean column wise
    centroid_A = np.mean(A, axis=1)
    centroid_B = np.mean(B, axis=1)

    # ensure centroids are 3x1
    centroid_A = centroid_A.reshape(-1, 1)
    centroid_B = centroid_B.reshape(-1, 1)

    # subtract mean
    Am = A - centroid_A
    Bm = B - centroid_B

    H = Am @ np.transpose(Bm)

    # find rotation
    U, S, Vt = np.linalg.svd(H)
    R = Vt.T @ U.T

    # sanity check
    if np.min(S) < 1e-10:
        print("careful! smallest singular value of H is: "+str(np.min(S)))


    # special reflection case
    if np.linalg.det(R) < 0:
        print("det(R) < 0, reflection detected!, correcting for it ...")
        Vt[2,:] *= -1
        R = Vt.T @ U.T

    t = -R @ centroid_A + centroid_B

    return R, t.reshape((3))

def pointertip(pointer_data:np.ndarray, rigid_body:np.ndarray) -> (np.ndarray): 
    """
    tip, rms_error = pointertip (pointer, rigid_body)
    
    Inputs
    ----------
    pointer : [3 x n_sample x 6] numpy array representing x,y,z coordinates of 
            pointer markers
    rigid_body : [3x6] numpy array representing xyz pointer marker locations 
                 such that the pointer tip is at [0,0,0]

    Process
    ------
    Uses rigid_transform_3D to find the tip location given the marker data. 
    Note 1: averages tip locations over samples (assuming tip location was same
                                               for all samples!!)
    Note 2: make sure dimensions of pointer and rigid body are the same!!
    
    Returns
    -------
    tip : 3x1 location of tip vector
    rms_error : rms over samples of the euclidian distance of tip to the 
    centroid position 
    
    Author: Koen Lemaire (k.k.lemaire2@vu.nl)
    
    """
    # preallocate tip matrix
    n_sample,n_dim,n_marker = pointer_data.shape
    tip=np.zeros((n_sample,n_dim))
    # loop over samples 
    for i in range(n_sample):
        R,tip[i]=rigid_transform_3D(rigid_body,pointer_data[i]) # calculate tip location
        
    mean_tip=np.mean(tip,axis=0)   

    # check on tip error    
    rms_tip=np.sqrt(np.mean(np.sum((tip-mean_tip)**2,axis=1)))
    assert rms_tip<0.001, 'pointer tip rms = '+str(rms_tip)+ ', check your pointer marker data' 

    return mean_tip 


def treadmill_frame(file_names,rigid_body):
    """
    R, origin, tip_locs = treadmill_frame(file_names,rigid_body):
    
    Inputs
    ----------
    file_names : list containing (absolute path) file names of the 5 pointer 
                 markers (!!see order below!!) .ndf files that define the 
                 treadmill reference frame
    rigid_body : [3x6] numpy array representing xyz pointer marker locations 
                 such that the pointer tip is at [0,0,0]

    This function defines the treadmill based reference frame based on rotation
    matrix R and origin such that for a vector v_kubus in the kubus based frame
        v_treadmill = R.T@(v_kubus - origin)
    
    
    ! STRICTLY adhere to below pointer locations, which are 'pointered' 
    locations in the kubus based reference frame. File names should be ordered 
    such that they adhere to the below order of pointered locations
    
    ! Orientation: right side is window side, left side is corridor side, front 
    is where your nose is pointing if the window is on your right. 
        
    location 0: treadmill surface
    location 1: back left screw (corridor side, closest to door)
    location 2: front left screw
    location 3: front right screw
    location 4: back right screw
    
    Defines the positive x-axis as pointing from location 1 to 4
    Defines the positive y-axis as pointing from location 1 to 3
    Defines the positive z-axis as up
    Defines the origin as location 1, but with the z-coordinate of location 0
    
    Returns
    -------
    R : 3x3 orthonormal rotation matrix describing the orientation of the 
    treadmill based reference frame with respect to the kubus based frame
    
    origin : 3x1 vector describing the location of the origin of the treadmill 
    based reference frame with respect to the kubus based frame 
    
    tip_locs: 3x5 xyz positions of pointered points in kubus based reference frame    
    
    Author: Koen Lemaire (k.k.lemaire2@vu.nl)
    
    """
    tip_locs=np.zeros((5,3)) # initialize
    
    # load data
    for iFile in range(5): # get tip locations
        print('processing pointer file: '+file_names[iFile])
        pointer_data,idx_good=readndf(file_names[iFile])[3:]
        if idx_good is not None: 
            pointer_data=pointer_data[idx_good,:,:]
        tip_locs[iFile]=pointertip(pointer_data, rigid_body)

    y_axis=tip_locs[2]-tip_locs[1] # forward y-axis
    x_axis=tip_locs[4]-tip_locs[1] # rightward x-axis
    z_axis=np.cross(x_axis,y_axis) # upward z-axis
    y_axis=np.cross(z_axis,x_axis) # make sure axes are orthogonal
    
    # normalize to unit length
    x_axis=x_axis/np.linalg.norm(x_axis)
    y_axis=y_axis/np.linalg.norm(y_axis)
    z_axis=z_axis/np.linalg.norm(z_axis)
    
    # these are now unit vectors of place in rotation matrix
    R_opto=np.vstack((x_axis, y_axis, z_axis)).T # transpose because of vstack 
    
    # origin of kubus reference frame in optotrack frame equals minus(!) 
    # first tip location
    origin_opto=tip_locs[1] 
    # origin_opto=np.mean(tip_locs[1:],axis=0)
    # define z coordinate with zeroth tip location
    origin_opto[2]=-tip_locs[0,2]  
    
    return R_opto, origin_opto, tip_locs

def change_frame(marker_data:np.ndarray, R:np.ndarray, origin:np.ndarray) -> (np.ndarray): 
    """
    marker_data = change_frame (marker_data, R, origin)
    
    Inputs
    ----------
    marker_data : [n_sample x n_dim x n_marker] numpy array representing x,y,z 
                    coordinates of markers in the local reference frame
    R : [3x3] 3x3 orthonormal rotation matrix describing the orientation of the 
              local reference frame with respect to the global frame    
    origin : 3x1 vector describing the location of the origin of the local
            reference frame with respect to the global frame

    Process
    ------
    changes marker_data such that marker_data=R*marker_data + origin
    
    Returns
    -------
    marker_data : xyz coordinates of marker_data in global frame 
    """
    # get shapes
    n_sample,n_dim,n_marker=marker_data.shape
    # define translation vector and transpose rotation matrix ahead of time
    t=origin.reshape((3,1))
    Rt=R.T
    for i in range(n_sample):
         marker_data[i,:,:]=Rt@(marker_data[i]-t)        
    return marker_data
    